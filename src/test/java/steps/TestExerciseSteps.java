package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class TestExerciseSteps {

    private HttpClient httpClient;
    private HttpResponse<String> response;
    private HttpRequest request;


    @Given("I call GET {string}")
    public void iCallGET(String url) {
        httpClient = HttpClient.newHttpClient();
        request = HttpRequest
                .newBuilder()
                .GET()
                .uri(URI.create(url))
                .build();
        System.out.println("The get url: " + url);
    }

    @Then("The response status should be {int}")
    public void theResponseStatusShouldBe(int statusCode) throws
            IOException,
            InterruptedException {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println("Response body: " + response.body());
            Assert.assertNotNull(response);
            Assert.assertEquals(statusCode, response.statusCode());
            System.out.println("The statusCode: " + response.statusCode());
    }

    @Then("The the response Contains : {string}")
    public void theTheResponseContains(String expected) {
        boolean isFound = response.body().toLowerCase().contains(expected.toLowerCase());
        Assert.assertTrue(isFound);
        System.out.println("The following value in the response where found: " + expected);
    }
}


