import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(  monochrome = true,
        tags = "@tags",
        features = "src/test/resources/features/",
        glue = {"steps"} )


public class TestExercise {

}
